#include "stdafx.h"
#include "utils.h"

#include <string>
#include <fstream>
#include <streambuf>


#define WIN32_LEAN_AND_MEAN

#include <curl\curl.h>
#include <json/json.hpp>
#include <boost\filesystem.hpp>

using namespace std;

using json = nlohmann::json;

void Utils::PrintSeparator()
{

	cout << "--------------------" << endl;

}

boost::filesystem::path Utils::GetCWD()
{

	return boost::filesystem::current_path();

}

wstring Utils::GetTempPathLocation()
{

	wstring tempPath;
	wchar_t wcharPath[MAX_PATH];

	if (GetTempPath(MAX_PATH, wcharPath))
		tempPath = wcharPath;

	return tempPath;

}

void Utils::UnzipFile(string a, string b)
{

	

}

string Utils::GETRequest(const char* url)
{

	char * result;
	CURL *curl;
	CURLcode res;
	curl = curl_easy_init();

	if (curl)
	{

		curl_easy_setopt(curl, CURLOPT_URL, url);

		string response;
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Utils::write_to_string);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);

		return response;

	}

	return "Error";

}

size_t Utils::write_to_string(void * ptr, size_t size, size_t count, void * stream)
{
	
	((string*)stream)->append((char*)ptr, 0, size * count);
	return size * count;

}

Utils::Configuration Utils::ReadConfig()
{

	Utils::Configuration config;

#ifndef _DEBUG

	ifstream infile{ "config.json" };
	string file_contents{ istreambuf_iterator<char>(infile), istreambuf_iterator<char>() };
 
	auto json = json::parse(file_contents);

	string hostname = json["hostname"];
	int serverPort = json["serverPort"];
	int updateFrequency = json["updateFrequency"];
	string computerName = json["computerName"];

	config.computerName = computerName;
	config.hostname = hostname;
	config.port = serverPort; 
	config.updateFrequency = updateFrequency;

#else

	config.computerName = "DEV";
	config.hostname = "localhost";
	config.port = 5555;
	config.updateFrequency = 10000;

#endif

	return config;

}