#pragma once

#include <string>

#include <boost\filesystem.hpp>

using namespace std;

class Utils
{
public:
	struct Configuration
	{

		string hostname;
		int port;
		int updateFrequency;
		string computerName;


	};
	void PrintSeparator();
	Configuration ReadConfig();
	string GETRequest(const char* url);
	void UnzipFile(string a, string b);
	boost::filesystem::path GetCWD();

	static wstring GetTempPathLocation();

private:
	static size_t write_to_string(void *ptr, size_t size, size_t count, void *stream);


};

