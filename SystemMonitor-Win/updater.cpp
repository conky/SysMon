#include "stdafx.h"
#include "updater.h"

#include "utils.h"
#include "constants.h"

#include <stdio.h>
#include <Windows.h>
#include <shellapi.h>

#include <curl\curl.h>
#include <json\json.hpp>
#include <boost\filesystem.hpp>

using json = nlohmann::json;

using namespace std;

size_t Updater::write_data(void *ptr, size_t size, size_t nmemb, FILE *stream)
{

	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;

}

void Updater::ExtractFiles()
{

	// move the client executable
	int result;
	char oldname[] = "SystemMonitor-Client.exe";
	char newname[] = "SystemMonitor-Client.exe.old";

	result = rename(oldname, newname);

	if (result != 0) {
		return;
	}

	Utils *util = new Utils();

	wstring tempPath = Utils::GetTempPathLocation();
	
	string tmpPath = string(tempPath.begin(), tempPath.end()) + "sysmon-update.zip";

	boost::filesystem::path path(util->GetCWD());

	//HRESULT zipFileExtraction = util->UnzipFile((LPCWSTR)tmpPath.c_str(),(LPCWSTR)path.c_str());

	delete util;

}

void Updater::DownloadUpdateFiles(string updateURL)
{

	CURL *curl;
	FILE *fp;
	CURLcode res;
	
	wstring tempPath = Utils::GetTempPathLocation();

	string fullPath = string(tempPath.begin(), tempPath.end()) + "sysmon-update.zip";

	curl = curl_easy_init();

	if (curl)
	{

		FILE *fp;
		fopen_s(&fp, fullPath.c_str(), "wb");

		curl_easy_setopt(curl, CURLOPT_URL, updateURL);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

		res = curl_easy_perform(curl);

		curl_easy_cleanup(curl);
		fclose(fp);

		ExtractFiles();

	}


}

Updater::UpdateInformation Updater::FetchUpdateInformation()
{

	Utils *utils = new Utils();

#ifdef _DEBUG
	string updateJSON = utils->GETRequest("http://localhost/update.json");
#else
	string updateJSON = utils->GETRequest("url here");
#endif

	auto json = json::parse(updateJSON);

	string version = json["version"];
	string md5 = json["md5"];
	string downloadURL = json["downloadurl"];

	UpdateInformation updateInfo;

	updateInfo.downloadURL = downloadURL;
	updateInfo.md5 = md5;
	updateInfo.version = version;

	delete utils;

	return updateInfo;

}

bool Updater::IsUpdateAvailable()
{

	Utils *utils = new Utils();

#ifdef _DEBUG
	string remoteVersion = utils->GETRequest("http://localhost/version.txt");
#else
	string remoteVersion = utils->GETRequest("url here");
#endif
	string currentVersion = Constants::GetVersion();

	if (currentVersion != remoteVersion)
	{

		delete utils;
		return true;

	}
	else {

		delete utils;
		return false;

	}

}
