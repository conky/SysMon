#include "stdafx.h"

#include <string>

#include "constants.h"
#include "utils.h"
#include "updater.h"

using namespace std;

Utils::Configuration config;

int main()
{

	cout << "SysMon v" << Constants::GetVersion().c_str() << endl;

	Utils *util = new Utils();
	util->PrintSeparator();
	
	cout << "Attempting to read config file...";

	config = util->ReadConfig();
	cout << "done." << endl;

	cout << "Checking for client updates...";

	Updater *updater = new Updater();

	if (updater->IsUpdateAvailable())
	{

		cout << "done. An update is available." << endl;
		util->PrintSeparator();
		Updater::UpdateInformation updateInfo = updater->FetchUpdateInformation();

		cout << "What would you like to do?" << endl;
		cout << "1. Perform update" << endl;
		cout << "2. Ignore update" << endl;

		string option;
		option = cin.get();

		if (option == "1")
		{

			// perform the update
			updater->DownloadUpdateFiles(updateInfo.downloadURL);

		}

	}
	else {

		cout << "done. No updates are available." << endl;

	}

	delete updater;
	delete util;

	cin.get();

    return 0;
}

