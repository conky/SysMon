#pragma once

#include <string>

using namespace std;

class Updater
{
public:
	struct UpdateInformation {

		string version;
		string md5;
		string downloadURL;

	};

	UpdateInformation FetchUpdateInformation();
	bool IsUpdateAvailable();
	void DownloadUpdateFiles(string updateURL);
	void ExtractFiles();

private:
	static size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream);

};

